#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import datetime
import re
import os
import shutil
import subprocess
import sys


def get_args():
    args = argparse.ArgumentParser(description="""
    ============================================================
    | Собирает данные из уже имеющихся инструментов в CSV файл |
    ============================================================
    """, formatter_class=argparse.RawTextHelpFormatter)
    today = datetime.datetime.today().strftime(format("%Y%m%d"))
    yesterday = (datetime.datetime.today() - datetime.timedelta(days=1)).strftime(format("%Y%m%d"))
    args.add_argument("-f", "--from", default=yesterday, help="Дата, с которой выбирать значения. "
                                                              "Формат: YYYYMMDD (По-умолчанию: вчера)")
    args.add_argument("-t", "--to", default=today, help="Дата, до которой выбирать значения. "
                                                        "Формат: YYYYMMDD (По-умолчанию: сегодня)")
    args.add_argument("-l", "--list", default="client_list.txt", help="Путь до текстового файла со списком клиентов"
                                                                      " (По-умолчанию: ./client_list.txt)")
    return args.parse_args()


def main():
    args = vars(get_args())
    arg_from = datetime.datetime.strptime(args["from"], "%Y%m%d").date()
    arg_list = args["list"]
    arg_to = datetime.datetime.strptime(args["to"], "%Y%m%d").date()
    customer_list = []
    script_path = "<good_script_path>"
    output_path = "result.csv"
    if not os.path.isfile(arg_list):
        print("Файл со списком клиентов не найден")
        sys.exit(0)
    if not re.match(r"^\d{8}$", datetime.datetime.strftime(arg_from, "%Y%m%d")) or \
            not re.match(r"^\d{8}$", datetime.datetime.strftime(arg_to, "%Y%m%d")):
        print("Введите даты в корректном формате: YYYYMMDD")
        sys.exit(0)
    with open(arg_list) as f:
        customer_list = f.read().splitlines()
    if not customer_list:
        print("Список клиентов пуст")
        sys.exit(0)
    if os.path.exists(output_path):
        shutil.copy2(output_path, f"{output_path}.bak")
    with open(output_path, mode="w") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(["Customer", "Date", "CPU usage", "Percona usage"])
        for c in sorted(customer_list):
            for d in range(int((arg_to - arg_from).days) + 1):
                d_txt = datetime.datetime.strftime(arg_from + datetime.timedelta(d), "%Y%m%d")
                result = dict(r.split() for r in
                              subprocess.check_output(f"{script_path} -c {c} -d {d_txt} -r", shell=True)
                              .decode(encoding="utf8").split(sep="\n")[:-1])
                d_csv = ".".join([d_txt[6:], d_txt[4:6], d_txt[:4]])
                csv_writer.writerow([c, d_csv, result["CPU:"], result["PERCONA:"]])


if __name__ == "__main__":
    sys.exit(main())
