import selenium.webdriver
import selenium.webdriver.common.desired_capabilities
import selenium.webdriver.support.expected_conditions
import tkinter

if __name__ == "__main__":
    url = r"http://<ucoz_site>/admin/"
    pass_file = r"C:\scripts\site.txt"
    passwd = open(pass_file).readline().strip()
    path_to_driver = r"C:\scripts\IEDriverServer.exe"

    capabilities = selenium.webdriver.common.desired_capabilities.DesiredCapabilities.INTERNETEXPLORER.copy()
    capabilities["ignoreProtectedModeSettings"] = True
    options = selenium.webdriver.IeOptions()
    options.set_capability(selenium.webdriver.IeOptions.NATIVE_EVENTS, False)
    browser = selenium.webdriver.Ie(executable_path=path_to_driver, capabilities=capabilities, options=options)

    try:
        browser.get(url)
        form = browser.find_element_by_name("password")
        form.clear()
        form.send_keys(passwd)
        # form_send =
        browser.find_element_by_xpath(r'//*[@id="subbutlform"]').click()
        tk_window = tkinter.Tk()
        tk_window.title("Close me")
        tk_window.geometry("300x50")
        tk_label = tkinter.Label(tk_window, text="Close the window")
        tk_label.pack()
        tk_label.mainloop()
    finally:
        browser.quit()