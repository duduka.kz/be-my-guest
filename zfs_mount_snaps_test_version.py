#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import operator
import os
import re
import subprocess
import sys


def main():
    last_backup_date = datetime.datetime.now() - datetime.timedelta(days=31)
    cmd_fs_list = "zfs list  -H -o name"
    pattern_fs = re.compile(r"here_was_my_pattern")
    server_list = [s.split(sep="/")[1] for s in subprocess.run(cmd_fs_list,
                                                               shell=True, capture_output=True,
                                                               text=True).stdout.rstrip().split(sep="\n")
                   if re.match(pattern_fs, s)]

    if not server_list:
        print(f"Файловые системы не найдены.\nПроверьте вывод команды: '{cmd_fs_list}'")
        sys.exit(0)

    cmd_snap_list = "zfs list -H -t snapshot -o name"
    snap_list = subprocess.run(cmd_snap_list, shell=True, capture_output=True,
                               text=True).stdout.rstrip().split(sep="\n")
    if not snap_list:
        print(f"Не найдено ни одного снапшота.\nКоманда для проверки: '{cmd_snap_list}'")
        sys.exit(0)

    pattern = re.compile(r"^here_was_my_pattern/(and|here|too)@\w+_\d{8}_\d{6}_\d{3}$")
    for snap in snap_list:
        if re.match(pattern, snap):
            server, kind, date_literal = operator.itemgetter(1, 3, 5)(re.split(r"[@/_]", snap))
            date = datetime.datetime.strptime(date_literal, "%Y%m%d")
            if date > last_backup_date and server in server_list:
                mount_path = f"/some/path/here/{datetime.datetime.strftime(date, '%j-%d.%b.%Y')}"
                if not os.path.exists(mount_path):
                    os.makedirs(mount_path, mode=0o755)
                cmd_mount = f"mount -t zfs {snap} {mount_path}"
                error = subprocess.run(cmd_mount, capture_output=True, shell=True, text=True).stderr.rstrip()
                if error:
                    print(error)


if __name__ == "__main__":
    sys.exit(main())
