#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import re
import paramiko
import subprocess
import sys


def get_args():
    args = argparse.ArgumentParser(description="""
    ===============================================
    | Перенос бэкапов с одного стораджа на другой |
    ===============================================
    """, formatter_class=argparse.RawTextHelpFormatter)
    args.add_argument("-n", "--node", required=True, help="Нода, для которой меняем сторадж")
    args.add_argument("-s", "--storage", required=True, help="Сторадж, на который переносим бэкапы")
    return args.parse_args()


def main():
    args = vars(get_args())
    errors_status = []
    errors_mounts = []
    files_missing = []
    home = os.path.expanduser("~")
    my_db = "<db>"
    my_host = "<server>"
    my_host_rw = "<another_server_o_O>"
    node = args["node"]
    path_backups = "<path>"
    storage_new = args["storage"]
    vol_names = []
    try:
        import pymysql
    except ImportError:
        subprocess.check_call([sys.executable, "-m", "pip", "install", "PyMySQL"])
        import pymysql
    my_connection = pymysql.connect(host=my_host, database=my_db, read_default_file="/".join([home, ".my.cnf"]))
    """Структура ответа:
    <она как суслик>
    """
    with my_connection:
        with my_connection.cursor() as my_cursor:
            my_cursor.execute(f"select <column> from <table> where <field> = '{node}';")
            storage_current = my_cursor.fetchone()[0]
            my_cursor.execute("select <column> from <table> where <field> = 34 ;")
            my_storages = [s[0] for s in my_cursor.fetchall()]
            my_cursor.execute(f"select <columns> from <table1> h "
                              f"inner join <table2> s on h.<table> = s.<table4> where s.<field1> = '{node}' and "
                              f"h.<field2> = 'Y';")
            my_result = my_cursor.fetchall()

    if not storage_current or storage_current == storage_new or storage_current not in my_storages or \
            storage_new not in my_storages:
        print("\nНекорректно выбран целевой сторадж или у ноды отсутствует сторадж в БД.\n")
        sys.exit(1)

    for r in my_result:
        if r[3] != "done":
            errors_status.append(r)
        if r[4] == "Y":
            errors_mounts.append(r)
    if errors_status or errors_mounts:
        try:
            import prettytable
        except ImportError:
            subprocess.check_call([sys.executable, "-m", "pip", "install", "prettytable"])
            import prettytable
        pretty_table = prettytable.PrettyTable(["<secret_list>"])
        print("\nСледующие вдс имеют смонтированные бэкапы или бэкапы в незаверёшнном состоянии:")
        for em in errors_mounts:
            pretty_table.add_row(em)
        for es in errors_status:
            pretty_table.add_row(es)
        print(pretty_table)
        print("Необходимо исправить или дождаться завершения бэкапа.\n")
        sys.exit(1)

    for r in my_result:
        vol_names.append(re.sub(r"<path>", "", r[2]))
    ssh_key = "/".join([home, ".ssh/id_rsa"])
    ssh_key_remote = "/tmp/del_id"
    ssh_command = f"screen -d -S transfer_backup -m bash -c 'cd {path_backups} ; " + \
                  f" rsync -e \"ssh -o StrictHostKeyChecking=no -i {ssh_key_remote}\" -aP " + " ".join(vol_names) + \
                  f" root@{storage_new}:{path_backups}/ ; rm {ssh_key_remote} " + " ".join(vol_names) + " ' "
    # rm apostrophe + " ".join(vol_names) + " ' "
    """ssh_command = " ".join((f"screen -d -S transfer_backup -m bash -c 'cd {path_backups} ; rsync -e "
                            f"\"ssh -o StrictHostKeyChecking=no -i {ssh_key_remote}\" -aP ", ssh_command,
                            f"root@{storage_new}:{path_backups}/ ; rm {ssh_key_remote} ' "))
    ssh_command = " ".join((f"screen -d -S transfer_backup -m bash -c 'cd {path_backups} ; rsync -e "
                            f"\"ssh -o StrictHostKeyChecking=no -i {ssh_key_remote}\" -aP ", ssh_command,
                            f"root@{storage_new}:{path_backups}/ ; rm {ssh_key_remote}", ssh_command, "'"))"""
    ssh_connection = paramiko.SSHClient()
    ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_connection.connect(storage_current, username="root", pkey=paramiko.RSAKey.from_private_key_file(ssh_key),
                           allow_agent=False)
    with ssh_connection:
        for v in vol_names:
            with ssh_connection.open_sftp() as scp_connection:
                try:
                    scp_connection.stat(f"<path>/{v}")
                except IOError:
                    files_missing.append(v)
        with ssh_connection.open_sftp() as scp_connection:
            scp_connection.put(ssh_key, ssh_key_remote)
        ssh_connection.exec_command(ssh_command)

    if files_missing:
        print("\n===================================================================================================")
        print("ВНИМАНИЕ! Следующие файлы резервных копий отсутствуют на сторадже. Необходимо проверить (через <program>),"
              " не остались ли они на другом сторадже после предыдущих переносов вдс, и перенести вручную. "
              "\nСписок:\n")
        for f in files_missing:
            print(f)
    my_connection = pymysql.connect(host=my_host_rw, database=my_db, read_default_file="/".join([home, ".my.cnf"]))
    with my_connection:
        with my_connection.cursor() as my_cursor:
            my_cursor.execute(f"update <table> set <field> = '{storage_new}' where <field2> = '{node}'")
            my_connection.commit()
            print(f"\n{node} была переключена на {storage_new}")
            print("\nНа ноде отмонтируй бэкапы и эталонные образы и прогони солт.\n")


if __name__ == "__main__":
    sys.exit(main())
