#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import csv
import datetime
import email
import pymysql
import os
import re
import smbclient
import smbprotocol.exceptions
import smtplib
import subprocess
import sys
import traceback


# очевидно, процедура отправки почты. Может вызываться в нескольких местах. Если вызвана при обработке исключений,
# останавливает работу скрипта
def send_mail(importance, email_body):
    message = email.message.EmailMessage()
    if importance:
        message['X-Priority'] = "2"
        message["Subject"] = "Импорт клиентской базы завершился с ошибкой."
    else:
        message["Subject"] = "Импорт клиентской базы завершился успешно."
    message["From"] = "<здесь был адрес>"
    message["To"] = "<и здесь тоже>"
    message.set_content(email_body)
    smtp_connection = smtplib.SMTP('localhost')
    smtp_connection.send_message(message)
    smtp_connection.quit()
    if importance:
        sys.exit(1)


def main():
    path_backups = r"/mnt/local_backup/db"
    path_smb_conf = r"/home/asterisk/.smbcredentials"
    path_my_cnf = r"/home/asterisk/.my.cnf"
    path_exported_file = r"\\<domain>\export\callers.csv"
    counter = 0
    days_backups = 14
    domain = "<domain>"
    my_params = {
        "host": "localhost",
        "user": "asterisk",
        "db": "call",
        "table": "callerid"
    }
    my_rows = ("ext", "caller", "managerLogin", "clientName", "clientURL")

    smb_credentials = configparser.ConfigParser()

    # ищем smb конфиг и используем параметры из него для подключения
    with open(path_smb_conf, "r") as config_smb:
        smb_credentials.read_string("[default]\n" + config_smb.read())
    smb_config = smbclient.ClientConfig(username=smb_credentials["default"]["user"],
                                        password=smb_credentials["default"]["password"], auth_protocol="ntlm")
    # https://github.com/jborean93/smbprotocol/issues/109
    # если просто прописать контроллер в конфиге, будет ошибка, поэтому приходится создавать объект
    # и задавать контроллер как параметр
    smb_config.domain_controller = domain

    # предварительно создаётся дамп текущей базы, чтобы было куда откатываться в случае отказа
    path_dump = path_backups + f"/{my_params['db']}-" + datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S") + ".sql"
    with open(path_dump, "w") as dump_file:
        try:
            subprocess.check_call(["mysqldump", f"{my_params['db']}", f"{my_params['table']}"], stdout=dump_file)
        except subprocess.CalledProcessError:
            send_mail(importance=True, email_body="Создание дампа текущей базы завершилось с ошибкой. "
                                                  "Выполнение задания прервано.\r\n\r\n"
                                                  "Данные для диагностики:\r\n" + ''.join(traceback.format_stack()))

    # проверяем, доступен ли файл с выгрузкой из 1с
    try:
        smbclient.stat(path_exported_file)
    except smbprotocol.exceptions.SMBException:
        send_mail(importance=True, email_body="Не удалось открыть файл на smb шаре. "
                                              "Выполнение задания прервано.\r\n\r\n"
                                              "Данные для диагностики:\r\n" + ''.join(traceback.format_stack()))

    # непосредственно, заливка базы
    with smbclient.open_file(path_exported_file, mode="r", encoding="utf-8-sig") as exported_file:
        # quoting нужно, чтобы убрать BOM в начале каждой строки, он не убирается даже с кодировкой выше
        csv_data = csv.reader(exported_file, delimiter=";", quoting=csv.QUOTE_ALL)
        # можно было бы через with, но мне нужна обработка исключений
        my_connection = pymysql.connect(host=my_params["host"], read_default_file=path_my_cnf,
                                        database=my_params["db"], autocommit=True)
        my_cursor = my_connection.cursor()
        try:
            my_cursor.execute(f"truncate table {my_params['table']};")
            for row in csv_data:
                # Умудряются выгрузить с меньше чем 5 полями
                if row and row[0] and len(row) >= 5 and row[0] != "ext":
                    if re.match(r"^[78]\d{10}$", row[1]):
                        row[1] = re.sub(r"^[78]", "", row[1])
                    my_query = f"insert into {my_params['table']} " \
                               f"({my_rows[0]}, {my_rows[1]}, {my_rows[2]}, {my_rows[3]}, {my_rows[4]}) values " \
                               f"('{row[0]}', '{row[1]}', '{row[2]}', '{row[3]}', '{row[4]}') ;"
                    my_cursor.execute(my_query)
                    counter += 1
                    # была идея выгружать только 10знаков в номере, но потом забил.
                    # Мало ли выйдут на международный рынок или cid изменится
                    """if re.match(r"^\d{10}$", row[1]):
                        my_cursor.execute(f"insert into test_table(1, 2, 3) values('%s', '%s, '%s')", row)
                    elif re.match(r"^[78]\d{10}$", row[1]):
                        row[1] = re.sub(r"^[78]", "", row[1])
                        my_cursor.execute("insert into test_table(1, 2, 3) values('%s', '%s, '%s')", row)"""
        except pymysql.Error:
            # если в процессе произошла какая-то ошибка связи с БД, залить дамп, созданный в начале
            # не поможет, правда, от падения mysql
            with open(path_dump, "rb") as dump_file:
                pipe = subprocess.Popen(["mysql", my_params["db"]], stdin=dump_file)
                pipe.wait()
            send_mail(importance=True, email_body="Загрузка csv завершилась с ошибкой, таблица перезалита из бэкапа."
                                                  "Данные для диагностики:\r\n" + ''.join(traceback.format_stack()))
        finally:
            my_cursor.close()
            my_connection.close()

    # чтобы не забивать дампами, удалить все старше n дней
    date_cleanup = datetime.datetime.today() - datetime.timedelta(days=days_backups)
    for file in os.listdir(path_backups):
        path_file = os.path.join(path_backups, file)
        if datetime.datetime.fromtimestamp(os.path.getmtime(path_file)) < date_cleanup and \
                os.path.splitext(file)[-1] == ".sql":
            os.remove(path_file)

    # если удалось досюда добраться, миссия выполнена
    send_mail(importance=False, email_body=f"Выгрузка базы клиентов завершена.\r\n"
                                           f"Общее количество импортированных записей: {counter}.")


if __name__ == "__main__":
    sys.exit(main())
