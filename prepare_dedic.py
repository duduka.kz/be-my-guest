#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import git
import paramiko
import os
import re
import subprocess
import sys


def get_args():
    args = argparse.ArgumentParser(description="""
    =============================================================
    | Проверка актуальности ОС на выделенном сервере с админкой |
    =============================================================
    """, formatter_class=argparse.RawTextHelpFormatter)
    args.add_argument("-n", "--dry-run", action='store_true',
                      help="Для тестирования, ничего не делает, только выводит информацию")
    args.add_argument("server", help="Имя выделенного сервера")
    return args.parse_args()


def check_server(server, dry_run):
    my_db = "<db>"
    my_server = "<server>"
    home = os.path.expanduser("~")
    my_cnf = "/".join([home, ".my.cnf"])

    try:
        import pymysql
    except ImportError:
        subprocess.check_call([sys.executable, "-m", "pip", "install", "PyMySQL"])
        import pymysql

    my_connection = pymysql.connect(host=my_server, database=my_db, read_default_file=my_cnf)
    with my_connection:
        with my_connection.cursor() as my_cursor:
            my_cursor.execute(f"select <columns> from <table> c join <table2> a on c.<field1>='{server}' "
                              "and c.<field2>=a.<field3> and a.<field4>='N' and c.<field5> not like '%@<domain>' ; ")
            result = my_cursor.fetchall()
            customers_db = [c[0] for c in result]
            my_cursor.execute(f"select <column> from <table1> a inner join <table2> c on c.<field1> = a.<field2> "
                              f"where c.<field3> = '{server}' "
                              f"and a.<field4> regexp '^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$' ; ")
            result = my_cursor.fetchall()
            ip_db = [i[0] for i in result]

    if customers_db:
        print(f"Найдены активные клиенты:\n{customers_db}\n. Выходим.\n")
        sys.exit(0)
    else:
        print("Активные клиенты согласно базы данных отсутствуют.")

    ssh_key_path = "/".join([home, ".ssh/id_rsa"])
    ssh_connection = paramiko.SSHClient()
    ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_connection.connect(server, username="<user>", pkey=paramiko.RSAKey.from_private_key_file(ssh_key_path),
                           allow_agent=False)
    with ssh_connection:
        i, o, e = ssh_connection.exec_command("grep CODENAME /etc/lsb-release | cut -d '=' -f 2")
        release = o.read().decode("utf-8").rstrip()
        i, o, e = ssh_connection.exec_command("egrep 'customers:|overloads:' /etc/group | "
                                              "cut -d ':' -f 4 | tr ',' '\n' | "
                                              "egrep -v '^$|<some_users>'| sort -r | uniq")
        customers_local = o.read().decode("utf-8").split()
        i, o, e = ssh_connection.exec_command("ip address show eth0 | grep 'inet ' | awk '{print $2}'")
        ip_local = [re.sub(r"/.*", "", i) for i in o.read().decode("utf-8").split()]

    ip_add = [i for i in ip_local if i in ip_db]

    ssh_connection.connect("<server>", username="<user>", pkey=paramiko.RSAKey.from_private_key_file(ssh_key_path),
                           allow_agent=False)
    with ssh_connection:
        i, o, e = ssh_connection.exec_command(f"find <path> -name '*<filename>*' | grep {release}")
        deploy_config = o.read()

    if deploy_config:
        print(f"На сервере {server} установлена Ubuntu {release}. Конфигурация для автодеплоя присутствует.")
    else:
        print(f"На сервере {server} установлена Ubuntu {release}. Конфигурация автодеплоя для этой версии не найдена. "
              f"Возможно, установлена не актуальная версия системы.")
        while True:
            reply = input("Продолжаем? (y/N) ")
            if not reply or reply in ("n", "N", "н", "Н"):
                print("Отмена...")
                sys.exit(0)
            elif reply in ("y", "Y", "д", "Д"):
                break
            else:
                print("Таки да или нет?")

    if customers_local:
        print("Клиенты на сервере присутствуют и будут удалены:")
        print(customers_local)
        if not dry_run:
            while True:
                reply = input("Продолжаем? (y/N) ")
                if not reply or reply in ("n", "N", "н", "Н"):
                    print("Отмена...")
                    sys.exit(0)
                elif reply in ("y", "Y", "д", "Д"):
                    break
                else:
                    print("Таки да или нет?")
    else:
        print("Клиенты на сервере отсутствуют.")

    ssh_connection.connect(server, username="<user>", pkey=paramiko.RSAKey.from_private_key_file(ssh_key_path),
                           allow_agent=False)
    with ssh_connection:
        i, o, e = ssh_connection.exec_command("<command>")
        for c in customers_local:
            # i, o, e = ssh_connection.exec_command(f"<command>")
            # subprocess.check_output(f"<secret_command>")
            # i, o, e = ssh_connection.exec_command(f"grep {c} /etc/passwd | cut -d ':' -f 1 | sort -r")
            # customers_add = o.read().decode("utf-8").split()
            # for c_a in customers_add:
            #     i, o, e = ssh_connection.exec_command(f"userdel -r {c_a}")
            i, o, e = ssh_connection.exec_command(f"grep {c} /etc/passwd | cut -d ':' -f 1 | sort -r")
            customers_add = o.read().decode("utf-8").split()
            call_portal = ["<secret_list>"]
            if dry_run:
                print(f"<command>")
                print(" ".join(call_portal))
                for c_a in customers_add:
                    print(f"userdel -r {c_a}")
            else:
                # pass
                i, o, e = ssh_connection.exec_command(f"<command>")
                print(" ".join(call_portal))
                subprocess.check_output(call_portal)
                for c_a in customers_add:
                    print(f"userdel -r {c_a}")
        for i in ip_add:
            if dry_run:
                print(f"Удаляется дополнительный адрес {i}")
            else:
                pass
                # i, o, e = ssh_connection.exec_command(f"ip address del {i}/24 dev eth0")

    my_connection = pymysql.connect(host=my_server, database=my_db, read_default_file=my_cnf)
    with my_connection:
        with my_connection.cursor() as my_cursor:
            my_cursor.execute(f"select <column> from <table> where <field> = '{server}' ; ")
            server_branch = my_cursor.fetchone()[0]

    if server_branch == "<branch>":
        server_branch = "<another_branch>"
    else:
        print(f"Сервер {server} находится не под основной веткой: {server_branch}")

    path_git = ""
    for root, dirs, files in os.walk(home, topdown=False):
        if re.match(r".*/<dir>$", root) and "<dir2>" in dirs:
            path_git = root
            break

    if path_git:
        repo = git.Repo(path_git)
        repo_git = repo.git
        repo_git.checkout(server_branch)
        repo_git.pull()
        server_files = []
        for root, dirs, files in os.walk(path_git, topdown=False):
            if re.match(rf".*/{server}$", root) and (files or dirs):
                server_files.append(root)
        """        continue
            for file in files:
                with open(os.path.join(root,file), "r") as f:
                    for line in f:
                        if server in line:
                            server_files.append(os.path.join(root,file))"""
        if server_files:
            print("Следующие файлы могут содержать изменения конфигурации сервера:")
            for f in server_files:
                print(f"{f}:", os.listdir(f))
        else:
            print(f"В git изменений конфигурации сервера {server} не найдено. "
                  f"Но это не точно, возможно, стоит проверить самостоятельно.")
    else:
        while True:
            reply = input("Не найден локальный репозиторий <repo>. Скачать в текущую папку? (y/N) ")
            if not reply or reply in ("n", "N", "н", "Н"):
                print("Отмена...")
                sys.exit(0)
            elif reply in ("y", "Y", "д", "Д"):
                break
            else:
                print("Таки да или нет?")
        repo = git.Repo.clone_from("<link>", "<name>")
        repo_git = repo.git
        repo_git.checkout(server_branch)
        server_files = []
        for root, dirs, files in os.walk(os.path.abspath(os.getcwd()) + "/<dir>", topdown=False):
            if re.match(rf".*/{server}$", root) and (files or dirs):
                server_files.append(root)
        if server_files:
            print("Следующие файлы могут содержать изменения конфигурации сервера:")
            for f in server_files:
                print(f"{f}:", os.listdir(f))
        else:
            print(f"В git изменений конфигурации сервера {server} не найдено. "
                  f"Но это не точно, возможно, стоит проверить самостоятельно.")


def main():
    args = vars(get_args())
    check_server(args["server"], args["dry_run"])


if __name__ == '__main__':
    sys.exit(main())
