#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import email
import re
import os
import platform
import shutil
import smtplib
import sys


def move_recordings(mr_src_dir, mr_dst_dir):
    search_pattern = "\.wav$"
    conf_file = "/etc/asterisk/pjsip.endpoint.conf"
    descriptions_tmp = [re.findall(r".*<\d{3}>$", line) for line in open(conf_file, mode="r", encoding="utf-8")]
    descriptions = [entry[0] for entry in descriptions_tmp if entry != []]
    numbers_sales = [re.search(r"\d{3}", entry).group(0) for entry in descriptions if re.search(r"ОП ", entry)]
    numbers_retail = [re.search(r"\d{3}", entry).group(0) for entry in descriptions if re.search(r"РТТ ", entry)]

    for root, dirs, files in os.walk(mr_src_dir, topdown=False):
        for f in files:
            f_path = os.path.join(root, f)
            if re.search(rf"{search_pattern}", f, re.IGNORECASE):
                # print(rf"{f}'s size: ",os.path.getsize(f_path))
                if os.path.getsize(f_path) == 44:
                    # print(f"{f_path} is going to be deleted")
                    os.remove(f_path)
                else:
                    f_year, f_month, f_day = get_date(os.path.getmtime(f_path))
                    # print(rf"{f}'s creation date: {f_year}/{f_month}/{f_day}")

                    try:
                        f_int_number = re.search(r"-\d{3}-", f).group(0)[1:-1]
                        if f_int_number in numbers_sales:
                            f_new_dir = os.path.join(mr_dst_dir, "ОП", f_year, f_month, f_day)
                        elif f_int_number in numbers_retail:
                            f_new_dir = os.path.join(mr_dst_dir, "РТТ", f_year, f_month, f_day)
                        else:
                            f_new_dir = os.path.join(mr_dst_dir, "Прочее", f_year, f_month, f_day)
                    except AttributeError:
                        f_new_dir = os.path.join(mr_dst_dir, "Прочее", f_year, f_month, f_day)

                    f_new_path = os.path.join(f_new_dir, f)
                    if not os.path.isdir(f_new_dir):
                        # print(f"Creating directory {f_new_dir}")
                        os.makedirs(f_new_dir)
                    # print(f"Moving {f_path} to {f_new_path}")
                    os.replace(f_path, f_new_path)
        if not dirs and not files:
            # print(f"Removing empty dir {root}")
            os.rmdir(root)


def get_date(gd_timestamp):
    gd_year = datetime.datetime.fromtimestamp(gd_timestamp).strftime("%Y")
    gd_month = datetime.datetime.fromtimestamp(gd_timestamp).strftime("%m")
    gd_day = datetime.datetime.fromtimestamp(gd_timestamp).strftime("%d")
    return gd_year, gd_month, gd_day


def check_free_space(cfs_dir_path):
    threshold_warning = 0.1
    threshold_alert = 0.05
    dir_path = cfs_dir_path
    disk_free = shutil.disk_usage(dir_path).free / shutil.disk_usage(dir_path).total
    if disk_free < threshold_alert:
        send_mail("alert")
        cleanup(threshold_alert, dir_path)
    elif disk_free < threshold_warning:
        send_mail("warning")


def send_mail(level):
    body = {"alert": "Во избежание исчерпания места на диске старые записи будут удалены.",
            "warning": "Требуется вмешательство администратора. В ближайшее время записи могут начать удаляться."}[level]
    host_name = platform.node()
    message = email.message.EmailMessage()
    message["Subject"] = f"На сервере {host_name} заканчивается место под записи разговоров."
    message["From"] = "<тут был адрес>"
    message["To"] = "<и тут был адрес>"
    message.set_content(body)
    smtp_connection = smtplib.SMTP('localhost')
    smtp_connection.send_message(message)
    smtp_connection.quit()


def cleanup(c_threshold, c_dir_path):
    dir_path = c_dir_path
    date_oldest = 0
    for root, dirs, files in os.walk(dir_path, topdown=False):
        # print("rdf: ", root, dirs, files)
        if files:
            t = [os.path.join(root, f) for f in files if files]
            # print("t: ", t)
            date_oldest_temp = os.path.getmtime(min(t, key=os.path.getmtime))
            if date_oldest == 0 or date_oldest > date_oldest_temp:
                date_oldest = date_oldest_temp
    # print(datetime.datetime.fromtimestamp(date_oldest))# + datetime.timedelta(days=4))

    date_cleanup = date_oldest
    disk_free = shutil.disk_usage(dir_path).free / shutil.disk_usage(dir_path).total
    while disk_free < c_threshold:
        date_cleanup += datetime.timedelta(days=1).total_seconds()
        for root, dirs, files in os.walk(dir_path, topdown=False):
            for f in files:
                f_path = os.path.join(root, f)
                if os.path.getmtime(f_path) < date_cleanup:
                    os.remove(f_path)
                    # print(f"rm {f_path}")
            if not dirs and not files:
                os.rmdir(root)
                # print(f"rm {root}")
        disk_free = shutil.disk_usage(dir_path).free / shutil.disk_usage(dir_path).total


def main():
    src_dir = r"/mnt/test"
    dst_dir = r"/mnt/recordings/shared"
    move_recordings(src_dir, dst_dir)


if __name__ == "__main__":
    sys.exit(main())
